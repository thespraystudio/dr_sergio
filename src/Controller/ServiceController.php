<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{
    /**
     * @Route("/especialidades", name="service")
     */
    public function index(): Response
    {
        return $this->render('service/index.html.twig', []);
    }

    /**
     * @Route("/especialidades/traumatologia-y-ortopedia", name="service_ortopedia")
     */
    public function indexOrtopedia(): Response
    {
        return $this->render('service/ortopedia.html.twig', []);
    }

    /**
     * @Route("/especialidades/gonartrosis", name="service_cirugia")
     */
    public function indexCirugia(): Response
    {
        return $this->render('service/cirugia.html.twig', []);
    }

    /**
     * @Route("/especialidades/artroscopia-de-rodilla", name="service_artroscopia")
     */
    public function indexArtroscopia(): Response
    {
        return $this->render('service/artroscopia.html.twig', []);
    }

    /**
     * @Route("/especialidades/protesis-de-rodilla", name="service_reemplazo")
     */
    public function indexReemplazo(): Response
    {
        return $this->render('service/reemplazo.html.twig', []);
    }

    /**
     * @Route("/especialidades/codo-de-tenista", name="service_lesiones")
     */
    public function indexLesiones(): Response
    {
        return $this->render('service/lesiones.html.twig', []);
    }

    /**
     * @Route("/especialidades/infiltracion-de-rodilla", name="service_infiltracion")
     */
    public function indexInfiltracion(): Response
    {
        return $this->render('service/infiltracion.html.twig', []);
    }

    /**
     * @Route("/especialidades/protesis-de-cadera", name="service_protesis")
     */
    public function indexProtesis(): Response
    {
        return $this->render('service/protesis.html.twig', []);
    }
}
