<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AilingController extends AbstractController
{
    /**
     * @Route("/padecimientos", name="ailing")
     */
    public function index(): Response
    {
        return $this->render('ailing/index.html.twig', [
            'controller_name' => 'AilingController',
        ]);
    }
}
