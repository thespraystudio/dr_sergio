<?php

namespace App\Controller;

use App\Entity\Testimonial;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestimonialsController extends AbstractController
{
    /**
     * @Route("/testimonios", name="testimonials")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Testimonial::class);
        $testimonials = $repository->findNewestTestimonials();
        return $this->render('testimonials/index.html.twig', [
            'controller_name' => 'TestimonialsController',
            'testimonials' => $testimonials
        ]);
    }
}
