<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\Mailer;

class ContacController extends AbstractController
{
    /**
     * @Route("/contacto", name="contact")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $message = (new \Swift_Message())
                    ->setSubject('Formulario de contacto')
                    ->setFrom('outbox@traumatologo-ortopedista.mx')
                    ->setTo('drgarciatenorio@gmail.com')
                    ->setBody(
                        $this->renderView(
                                'layout/_mailing.html.twig', array('entity' => $data)
                        ), 'text/html'
                    );

            if ($mailer->send($message)) {
                $response = array('status' => "ok");
            } else {
                $response = array('status' => "error");
            }

            $jsonResponse = new JsonResponse($response);
            return $jsonResponse;

        }

        return $this->render('contac/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
