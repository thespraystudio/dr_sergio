<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Service\MarkdownHelper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * @Route("/blog")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("/", name="blog")
     */
    public function index(EntityManagerInterface $entityManager, Request $request): Response
    {

        $repository = $entityManager->getRepository(Post::class);
        $categoryRepository = $entityManager->getRepository(Category::class);

        $posts = $repository->findNewestPosts();
        $categories = $categoryRepository->findAll();


        return $this->render('blog/index.html.twig', [
            'posts' => $posts,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/page/{number}", name="blog_page_number")
     */
    public function page($number, EntityManagerInterface $entityManager, Request $request): Response
    {

        $repository = $entityManager->getRepository(Post::class);


        if ($request->isXmlHttpRequest()) {
            $posts = $repository->findNewestPosts($number);
        }
        return $this->render('blog/_columns.html.twig', [
            'posts' => $posts, 'number' => $number + 1
        ]);
    }


    /**
     * @Route("/{slug}", name="blog_post_show", requirements={"slug"=".+"})
     */
    public function show($slug, EntityManagerInterface $entityManager)
    {
        $repository = $entityManager->getRepository(Post::class);
        /** @var Post|null $blogPost */
        $blogPost = $repository->findOneBy(['slug' => $slug]);
        if (!$blogPost) {
            throw $this->createNotFoundException(sprintf('no post found for slug "%s"', $slug));
        }


        return $this->render('blog/show.html.twig', [
            'blogPost' => $blogPost
        ]);
    }
}
