<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Cita;
use App\Form\CitaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\Mailer;
use Symfony\Component\String\Slugger\SluggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\File;

class CitasController extends AbstractController {

    /**
     * @Route("/citas", name="app_citas")
     */
    public function index(EntityManagerInterface $entityManager, Request $request, SluggerInterface $slugger, \Swift_Mailer $mailer): Response {

        $cita = new Cita();
        $formCitas = $this->createForm(CitaType::class, $cita);
        $formCitas->handleRequest($request);

        if ($formCitas->isSubmitted() && $formCitas->isValid()) {

            $file = $formCitas->get('filename')->getData();
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
                try {
                    $file->move(
                            $this->getParameter('filenames_directory'),
                            $newFilename
                    );
                } catch (FileException $e) {
                    
                }
                $cita->setFilename($newFilename);
            }
            if ($formCitas->isSubmitted() && $formCitas->isValid()) {
                $data = $formCitas->getData();

                $message = (new \Swift_Message())
                        ->setSubject('Formulario de contacto')
                        ->setFrom('outbox@traumatologo-ortopedista.mx')
                        ->setTo('drgarciatenorio@gmail.com')
                        ->attach(\Swift_Attachment::fromPath('/uploads/archivos/' . $newFilename))
                        ->setBody(
                        $this->renderView(
                                'layout/_cita.html.twig', array('entity' => $data)
                        ), 'text/html'
                );

                if ($mailer->send($message)) {
                    $response = array('status' => "ok");
                } else {
                    $response = array('status' => "error");
                }
                $jsonResponse = new JsonResponse($response);
                $entityManager->persist($cita);
                $entityManager->flush();



                return $this->redirectToRoute('home');
            }
        }


        return $this->render('citas/index.html.twig', [
                    'controller_name' => 'CitasController',
                    'form' => $formCitas->createView(),
        ]);
    }

}
