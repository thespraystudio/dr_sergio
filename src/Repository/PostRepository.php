<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @return Post[] Returns an array of Post objects
     */
    public function findNewestPosts($page = 0)
    {
        $limit = 9;
        $offset = $page * $limit;
        return $this->createQueryBuilder('p')
            ->andWhere('p.category IS NOT NULL')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults(9)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }
}
