<?php

namespace App\Form;

use App\Entity\Cita;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\HttpFoundation\File\File;

class CitaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre',null,[
                'label'=>'Nombre'
            ])
            ->add('telefono',null,[
                'label'=>'Teléfono'
            ])
            ->add('situacion',null,[
                'label'=>'Situación'
            ])
            ->add('filename', FileType::class, [
                'label' => 'Sube tu archivo de evidencia',
                'mapped' => false,
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cita::class,
        ]);
    }

}
