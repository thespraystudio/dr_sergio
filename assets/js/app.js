import '../css/app.scss';
// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import 'bootstrap';
import 'slick-carousel/slick/slick';
import 'jquery-validation';
import listIcon from '../images/home/vi.png'
import rodillaIcon from '../images/icons/rodilla_blue.svg'
import hombroIcon from '../images/icons/hombro_blue.svg'
import columnaIcon from '../images/icons/columna_blue.svg'
import caderaIcon from '../images/icons/cadera_blue.svg'
import tobilloIcon from '../images/icons/tobillo_blue.svg'
import pieIcon from '../images/icons/pie_blue.svg'
import otrosIcon from '../images/icons/otros_blue.svg'
import codoIcon from '../images/icons/codo_blue.svg'
import munecaIcon from '../images/icons/muneca_blue.svg'
import caderaActive from '../images/icons/cadera-active.svg'
import columnaActive from '../images/icons/columna-active.svg'
import hombroActive from '../images/icons/hombro-active.svg'
import rodillaActive from '../images/icons/rodilla-active.svg'
import pieActive from '../images/icons/pie-active.svg'
import InfiniteAjaxScroll from '@webcreate/infinite-ajax-scroll';

import Shuffle from "shufflejs";

window.jQuery = $;

onresize = (event) => {
    detectDevice();
    toggleMenuBtn();
    calcMenuMobile();
};

if ($('.blog-posts-wrap').length) {
    let ias = new InfiniteAjaxScroll('.blog-posts-wrap', {
        item: '.post-entry-wrap',
        next: '.next',
        pagination: '.pagination'
    });
}
$(document).ready(function () {
    let sliders = {};

    detectDevice();
    toggleMenuBtn();
    calcMenuMobile();


    /* VALIDACIÓN DE FORMULARIOS */
    $.each($('form.jvalidate'), function (key, value) {
        var fields_string = "";
        var error_container = $(this).next('.error-list');
        $(this).validate({
            submitHandler: function (form) {
                $(form).fadeTo('fast', .30);
                var data = $(form).serialize();
                $(form).find('input').attr('disabled', 'disabled');
                $(form).find('textarea').attr('disabled', 'disabled');
                $.ajax({
                    type: $(form).attr('method'),
                    url: $(form).attr('action'),
                    data: data,
                    dataType: 'json'
                }).always(function () {
                    $(form).find('input').removeAttr('disabled');
                    $(form).find('textarea').removeAttr('disabled');
                })
                    .done(function (response) {
                        $(form).find('input[type="text"]').val('');
                        $(form).find('textarea').val('');
                        $(form).find('input[type="checkbox"]').prop('checked', false);
                        $(form).fadeTo('fast', 1);
                        if (response.status === 'ok') {
                            $(form).fadeOut();
                            $(form).parent().append('<h1 class="w-100 thanks text-center">¡Gracias!</h1><h2 class="text-center w-100 submited">Formulario enviado correctamente</h2>');
                        }
                    });
            },
            wrapper: "div"
        });
    });

    if ($(".js-slick").length) {
        $(".js-slick").each(function (index, value) {
            var sliderName = $(this).data('slider-name') || 'slider-' + index;
            let sliderOptions = {
                dots: $(this).data('slider-dots') || false,
                infinite: $(this).data('slider-infinite') || true,
                slidesToShow: $(this).data('slider-slides-show') || 1,
                centerMode: $(this).data('slider-center-mode') || false,
                variableWidth: $(this).data('slider-variable-width') || false,
            }
            sliders[sliderName] = $(this).slick(sliderOptions);
        });
    }

    $('.body-dot').click(function (event) {
        $('.body-dot').removeClass('active');
        $(this).addClass('active');

        var bodyPart = $(this).data('body');
        changeAilings(bodyPart, $(this));
    });

    $('.ailing-list').on('click', 'li', function (event) {
        $('.ailing-list li').removeClass('active');
        $(this).addClass('active');

        $('.ailing-example, .description').fadeOut('300');


        var thisE = $(this);
        setTimeout(function () {
            var ailingDescription = thisE.data('description');
            $('.description').text(ailingDescription);

            var ailingPreview = thisE.data('image');
            $('.preview').attr('src', ailingPreview);

            var ailingName = thisE.text();
            $('.preview-name').text(ailingName);
            //$('.description').text(gAilings[0]['description']);


            $('.ailing-example, .description').fadeIn('300');
        }, 300);
    });


    function changeAilings(bodyPart, elmnt) {
        switch (bodyPart) {
            case 'Rodilla':
                var ailings = [
                    {ailing: 'Artrosis de rodilla', description: 'La artrosis de rodilla es una enfermedad degenerativa y de "desgaste" que se presenta con mayor frecuencia en personas de edad avanzada, pero que también puede ocurrir en personas más jóvenes debido a lesiones previas en la rodilla. El cartílago se desgasta gradualmente disminuyendo la protección de los huesos.', image: '../ailings/image021.jpg'},
                    {ailing: 'Lesión condral', description: 'La lesión condral o lesión de cartílago es aquella que se produce en el cartílago articular, el tejido que recubre al hueso de nuestras articulaciones. Este tejido facilita el engranaje articular y protege la articulación del rozamiento excesivo.', image: '../ailings/image022.jpg'},
                    {ailing: 'Lesión meniscal', description: 'Una rotura del menisco es una de las lesiones de rodilla más comunes. Cualquier actividad que haga que tuerzas o gires la rodilla con fuerza, especialmente al poner todo tu peso sobre ella, puede llevar a que ocurra una rotura del menisco.', image: '../ailings/image023.jpg'},
                    {ailing: 'Ruptura de ligamento cruzado', description: 'Existen dos tipos de ligamentos cruzados: Ligamento cruzado anterior (LCA), evita que la tibia se desplace hacia delante con respecto al fémur. Ligamento cruzado posterior (LCP), su función es impedir que la tibia se desplace hacia atrás. El mecanismo más frecuente de rotura del ligamento cruzado es un giro de la rodilla brusco y descontrolado.', image: '../ailings/image024.jpg'},
                    {ailing: 'Lesiones ligamentarias', description: 'Los ligamentos conectan los huesos entre sí. Los que están por fuera de la articulación de la rodilla se denominan ligamento lateral interno y ligamento lateral externo. Estos sustentan la rodilla, proporcionando estabilidad y limitando el movimiento lateral. Las lesiones son muy frecuentes en deportes cuya práctica exige cambios frecuentes de ritmo y dirección, por ejemplo, en futbolistas.', image: '../ailings/image025.jpg'},
                    {ailing: 'Tendinitis', description: 'La tendinitis rotuliana, también conocida como “rodilla de saltador”, es una lesión en el tendón que conecta la rótula con la tibia, es muy común en atletas cuyos deportes incluyen saltos frecuentes, como el baloncesto y el voleibol.', image: '../ailings/image026.jpg'},
                    {ailing: 'Luxación de la rotula', description: 'La luxación de la rótula es el tipo de luxación mas frecuente de la rodilla y consiste en que la rótula se desplaza súbitamente hacia lateral abandonando su posición normal en la zona anterior del fémur.', image: '../ailings/image027.png'}
                ]
                var bodyIcon = rodillaIcon;
                var ailingImgActive = rodillaActive;

                showFront();
                break;
            case 'Hombro':
                var ailings = [
                    {ailing: 'Hombro Congelado', description: 'El hombro congelado es una enfermedad que provoca una disminución progresiva del movimiento del hombro por inflamación y retracción de los ligamentos de la articulación glenohumeral.', image: '../ailings/image001.png'},
                    {ailing: 'Ruptura de Manguito Rotador', description: 'Un desgarro en el manguito de los rotadores ocurre cuando uno de los tendones se desprende del hueso a raíz de una sobrecarga o lesión.', image: '../ailings/image002.png'},
                    {ailing: 'Tendinitis del Bíceps', description: 'La tendinitis del bíceps es la inflamación que sucede en la porción larga del tendón del bíceps. Aparece como un dolor en el hombro, concretamente en la parte delantera y asocia debilidad.', image: '../ailings/image003.jpg'},
                    {ailing: 'Luxación de hombro', description: 'Es el desencajamiento total de la articulación formada entre la cabeza del húmero y la cavidad glenoidea.', image: '../ailings/image004.png'},
                    {ailing: 'Artrosis glenohumeral', description: 'La Artrosis Glenohumeral es una enfermedad que lesiona el cartílago articular del hombro y origina dolor, rigidez e incapacidad funcional.', image: '../ailings/image005.jpg'},
                ]
                var bodyIcon = hombroIcon;
                var ailingImgActive = hombroActive;

                showFront();
                break;
            case 'Codo':
                var ailings = [
                    {ailing: 'Epicondilitis o codo de tenista', description: 'El codo del tenista (epicondilitis lateral) es una afección dolorosa que ocurre cuando los tendones del codo se sobrecargan, frecuentemente por movimientos repetitivos de la muñeca y el brazo.', image: '../ailings/image006.jpg'},
                    {ailing: 'Epitrocleítis o codo de golfista', description: 'La epitrocleitis, también llamada codo de golfista o epicondilitis medial, es la denominación que se le da a una enfermedad del codo en la cual se produce una tendinitis en la inserción de los músculos epitrocleares.', image: '../ailings/image007.jpg'},
                    {ailing: 'Artrosis del codo', description: 'La artrosis de codo es una afección que puede llevar al deterioro del cartílago articular del codo.', image: '../ailings/image008.jpg'},
                    {ailing: 'Bursitis de olecranon', description: 'La bursitis es una inflamación de pequeños sacos de líquido (bursas) que ayudan a que las articulaciones se muevan con facilidad. La bursitis del olécranon, que afecta la bursa del olécranon en la parte posterior del codo, se llama a veces codo de Popeye.', image: '../ailings/image009.jpg'},
                    {ailing: 'Codo gotoso', description: 'Los tofos gotosos son depósitos voluminosos de cristales de ácido úrico que se desarrollan en el tejido cartilaginoso, tendones y tejidos blandos. Y una de sus localizaciones es el codo.', image: '../ailings/image010.jpg'},
                ]
                var bodyIcon = codoIcon;

                showFront();
                break;
            case 'Muñeca':
                var ailings = [
                    {ailing: 'Síndrome del túnel del Carpo', description: 'Se presenta como un entumecimiento y hormigueo en la mano y el brazo, ocasionados por la compresion excesiva de un nervio en la muñeca.', image: '../ailings/image011.jpg'},
                    {ailing: 'Esguince de muñeca', description: 'Cuando usted sufre un esguince de muñeca, es porque se han estirado o roto uno o más de los ligamentos de la articulación de la muñeca. Esto puede ocurrir a raíz de un mal aterrizaje sobre su mano al caerse.', image: '../ailings/image012.jpg'},
                    {ailing: 'Quiste sinovial', description: 'El quiste sinovial es un nódulo formado por acumulación de líquido, que aparece normalmente en la mano y la muñeca.', image: '../ailings/image013.jpg'},
                    {ailing: 'Fascitis Palmar', description: 'La fascitis palmar es una enfermedad inflamatoria poco frecuente que se caracteriza por la deformidad en la flexión de los dedos de la mano, secundaria al engrosamiento de la fascia o membrana que recubre los tendones.', image: '../ailings/image014.jpg'},
                    {ailing: 'Tenosinovitis', description: 'La tenosinovitis De Quervain es una afección dolorosa que afecta los tendones de la muñeca del lado del pulgar. Si tienes tenosinovitis De Quervain, probablemente sientas dolor al girar la muñeca, agarrar cualquier cosa o cerrar el puño.', image: '../ailings/image015.jpg'},
                ]
                var bodyIcon = munecaIcon;

                showFront();
                break;
            case 'Cadera':
                var ailings = [
                    {ailing: 'Desgaste de cadera', description: 'La coxartrosis o artrosis de cadera consiste en el desgaste de la articulación de la cadera como consecuencia de su uso, envejecimiento, traumatismo o fracturas.', image: '../ailings/image016.jpg'},
                    {ailing: 'Pinzamientos', description: 'El pinzamiento de cadera es una condición en la cual el hueso del reborde de la pelvis (acetábulo) entra en contacto con la cabeza o cuello del fémur. Esto se produce por una combinación de crecimiento de hueso más allá de lo normal y el rango de movimiento que la persona le exige a su cadera.', image: '../ailings/image017.jpg'},
                    {ailing: 'Bursitis', description: 'Bursitis es la inflamación de la bolsa sinovial. Una bolsa cubre la protuberancia ósea del hueso de la cadera, llamada trocánter mayor, la otra bolsa, es del psoas-ilíaco,que está ubicada en la parte interna (lado de la ingle) de la cadera.', image: '../ailings/image018.jpg'},
                    {ailing: 'Epifisiolistesis femoral', description: 'La epifisiolistesis femoral proximal o deslizamiento epifisario femoral proximal consiste en un desplazamiento de la epífisis sobre la metáfisis a través del cartílago de crecimiento.', image: '../ailings/image020.jpg'},
                ]
                var ailingImgActive = caderaActive;
                var bodyIcon = caderaIcon;

                showFront();
                break;
            case 'Pie':
                var ailings = [
                    {ailing: 'Fascitis plantar', description: 'La fascitis plantar es la inflamación del tejido fibroso (fascia plantar), que abarca la planta del pie, desde el talón hasta los dedos. El estar mucho tiempo de pie, correr y el peso excesivo pueden deteriorar la fascia.', image: '../ailings/image032.jpg'},
                    {ailing: 'Juanetes (Hallux Valgus)', description: 'Un juanete, también conocido como hallux valgus , es una deformidad de la articulación que conecta el dedo gordo del pie con el pie. El dedo gordo del pie a menudo se dobla hacia los otros dedos y la articulación se enrojece y duele.', image: '../ailings/image033.jpg'},
                    {ailing: 'Pie plano', description: 'El pie plano es aquel cuya estructura presenta un aplanamiento o disminución del arco plantar. Dicho aplanamiento suele ser más notable cuando el paciente se encuentra de pie o caminando.', image: '../ailings/image034.jpg'},
                    {ailing: 'Síndrome del túnel del tarso', description: 'El síndrome del túnel del tarso es el dolor a lo largo del trayecto del nervio tibial posterior, generalmente debido a una compresión del nervio dentro del túnel del tarso.', image: '../ailings/image035.png'},
                    {ailing: 'Neuroma de Morton', description: 'El neuroma de Morton (también llamado neuroma interdigital o neuroma plantar) es una degeneración del nervio digital plantar acompañada de una fibrosis (engrosamiento) alrededor del nervio. Usualmente se localiza entre el 3º y 4º metatarsiano (75% de los casos). El paciente se queja de un dolor mecánico, similar a una descarga eléctrica que aumenta por la tarde o de un dolor acentuado después de mantenerse de pie durante un tiempo.', image: '../ailings/image036.jpg'},
                ]
                var ailingImgActive = pieActive;
                var bodyIcon = pieIcon;

                showFront();
                break;
            case 'Tobillo':
                var ailings = [
                    {ailing: 'Esguince de tobillo', description: 'Los esguinces de tobillo ocurren cuando los ligamentos que sujetan el tobillo se distienden demasiado y/o se desgarran. Un esguince de tobillo puede ocurrir al meter sin querer el pie dentro de un hoyo, cuando un tobillo gira hacia un lado mientras se camina o se corre o, simplemente, al apoyar mal el pie.', image: '../ailings/image029.jpg'},
                    {ailing: 'Espolón calcáneo', description: 'El espolón calcáneo es un crecimiento del hueso del talón en forma triangular o de lanza. Se produce cuando la fascia plantar (tejido elástico que une el talón con la zona de debajo de los dedos del pie) está sometida a excesiva tracción y sobrecarga generando unas microrroturas en su inserción. Esta parte se calcifica formándose esa especie de hueso que provoca un dolor agudo en el talón, como si fuesen pinchazos.', image: '../ailings/image030.jpg'},
                    {ailing: 'Tendinitis de Aquiles', description: 'Una tendinitis de Aquiles es una inflamación (irritación e hinchazón) del tendón de Aquiles. El tendón de Aquiles es una tira de tejido ubicada en la parte posterior del pie. Conecta el hueso del talón con los músculos de la pantorrilla.', image: '../ailings/image031.jpg'},
                ]
                var bodyIcon = tobilloIcon;

                showFront();
                break;
            case 'Columna':
                var ailings = [
                    {ailing: 'Dolor Cervical', description: 'La cervicalgia es el dolor en la región cervical que puede extenderse al cuello, cabeza o a la extremidad superior, que limita los movimientos y que se puede acompañar de disfunción neurológica. En la mayoría de las ocasiones no es grave y suele ser el resultado de una sobrecarga o un sobreesfuerzo de los músculos del cuello, o de una lesión neuromuscular traumática, como el «latigazo cervical.', image: '../ailings/image037.jpg'},
                    {ailing: 'Lumbalgia', description: 'La lumbalgia se define como dolor muscular en la zona lumbar (L1-L5), que conlleva un aumento del tono y de la rigidez muscular. Se trata de un dolor local acompañado de dolor referido o irradiado que no se produce como consecuencia de fracturas o traumatismos.', image: '../ailings/image038.jpg'},
                    {ailing: 'Hernia discal', description: 'Ocurre cuando todo o parte de un disco de la columna es forzado a pasar a través de una parte debilitada del disco. Esto puede ejercer presión sobre los nervios cercanos o la médula espinal. Esto presenta dolor, entumecimiento o debilidad. La parte baja (región lumbar) de la columna es el área más comúnmente afectada por una hernia de disco. Los discos del cuello (cervicales) están en la segunda área más comúnmente afectada.', image: '../ailings/image039.jpg'},
                    {ailing: 'Ciatalgia', description: 'La ciatalgia es el dolor en el trayecto del nervio ciático. Suele deberse a la compresión de las raíces nerviosas lumbares en la zona lumbar. Las causas más comunes son una hernia de disco intervertebral, osteofitos y estrechez del conducto espinal (estenosis espinal).', image: '../ailings/image040.jpg'},
                    {ailing: 'Radiculopatía', description: 'Una radiculopatía es causada por compresión, inflamación y/o lesión de una raíz de nervio espinal en la zona baja de la espalda. Las causas de este tipo de dolor, en orden de prevalencia, incluyen: Hernia de disco con compresión de nervio, La estenosis foraminal, Diabetes, Lesiones de la raíz nerviosa, Tejido cicatricial de una cirugía previa de la columna vertebral.', image: '../ailings/image041.jpg'},
                    {ailing: 'Síndrome facetario', description: 'El síndrome facetario es dolor o disfunción de las articulaciones facetarias de las vértebras. Es más común en las lumbares. Los síntomas son dolor en la parte baja de la espalda.', image: '../ailings/image042.jpg'},
                ]
                var bodyIcon = columnaIcon;
                var ailingImgActive = columnaActive;

                $('.body-map').removeClass('front');
                $('.body-map').addClass('back');
                break;
            case 'Otros':
                var ailings = [
                    {ailing: 'Fracturas', description: 'Una fractura es una grieta o una rotura de un hueso. La mayoría de las fracturas son consecuencia de la fuerza aplicada a un hueso.', image: '../ailings/image043.jpg'},
                    {ailing: 'Luxaciones', description: 'Una luxación es una separación de dos huesos en el lugar donde se juntan, es decir, en la articulación. Se denomina articulación luxada a aquella en la que los huesos ya no están en su posición normal.', image: '../ailings/image044.jpg'},
                    {ailing: 'Osteoartrosis', description: 'La osteoartrosis, también conocida como artrosis, osteoartritis o artritis hipertrófica, es una enfermedad degenerativa que se caracteriza por el desgaste del cartílago articular, lo que puede causar dolor y rigidez de las articulaciones.', image: '../ailings/image046.jpg'},
                    {ailing: 'Roturas musculares', description: 'La lesión muscular se produce habitualmente cuando una contracción o una elongación forzadas del músculo causan la rotura de las fibras musculares, y su grado varía desde las simples agujetas a las roturas completas del músculo.', image: '../ailings/image047.jpg'},
                    {ailing: 'Contusiones', description: 'Una contusión es un tipo de lesión física no penetrante sobre un cuerpo causada por la acción de objetos duros, de superficie obtusa o roma.', image: '../ailings/image048.jpg'},
                ]
                var bodyIcon = otrosIcon;

                showFront();
                break;
            default:
                var ailings;
        }
        //gAilings = ailings;

        $('.ailing-img').each(function (i, obj) {
            var inactiveImg = $(this).data('inactive');

            $(this).attr('src', inactiveImg);
        });
        elmnt.find('.ailing-img').attr('src', ailingImgActive);

        $('.ailings-fade').fadeOut('300')

        if ($('.ailing-list').hasClass('home'))
            var time = 0;
        else
            var time = 300;

        setTimeout(function () {
            $('.body-part-title').text(bodyPart);
            $('.body-icon').attr('src', bodyIcon);
            $('.description').text(ailings[0]['description']);

            $('.preview').attr('src', ailings[0]['image']);
            $('.preview-name').text(ailings[0]['ailing']);

            $('.ailing-list').html('');
            if ($('.ailing-list').hasClass('home')) {
                for (var i = 0; i < ailings.length; i++) {
                    $('.ailing-list').append('<li><img src="' + listIcon + '" class="img-fluid"/>' + ailings[i]['ailing'] + '</li>');
                }
            } else {
                for (var i = 0; i < ailings.length; i++) {
                    $('.ailing-list').append('<li data-description="' + ailings[i]['description'] + '" data-image="' + ailings[i]['image'] + '">' + ailings[i]['ailing'] + '</li>');
                }
                $('.ailing-list li:first-child').addClass('active');
            }
            $('.ailings-fade').fadeIn('300');
        }, time);

    }

    function showFront() {
        $('.body-map').removeClass('back');
        $('.body-map').addClass('front');
    }
});


class CategoryFilter {
    constructor(element) {
        this.categories = Array.from(document.querySelectorAll('.categories-filter-wrap button'));

        this.shuffle = new Shuffle(element, {
            easing: 'cubic-bezier(0.165, 0.840, 0.440, 1.000)',
        });

        this.filters = {
            categories: [],
        };

        this._bindEventListeners();
    }

    /**
     * Bind event listeners for when the filters change.
     */
    _bindEventListeners() {
        this._onCategoryChange = this._handleCategoryChange.bind(this);

        this.categories.forEach((button) => {
            button.addEventListener('click', this._onCategoryChange);
        }, this);
    }

    /**
     * Get the values of each `active` button.
     * @return {Array.<string>}
     */
    _getCurrentCategoryFilters() {
        return this.categories.filter((button) => button.classList.contains('active')).map((button) => button.dataset.value);
    }

    /**
     * A color button was clicked. Update filters and display.
     * @param {Event} evt Click event object.
     */
    _handleCategoryChange(evt) {
        const button = evt.currentTarget;

        // Treat these buttons like radio buttons where only 1 can be selected.
        if (button.classList.contains('active')) {
            button.classList.remove('active');
        } else {
            this.categories.forEach((btn) => {
                btn.classList.remove('active');
            });

            button.classList.add('active');
        }

        this.filters.categories = this._getCurrentCategoryFilters();
        this.filter();
    }

    /**
     * Filter shuffle based on the current state of filters.
     */
    filter() {
        if (this.hasActiveFilters()) {
            this.shuffle.filter(this.itemPassesFilters.bind(this));
        } else {
            this.shuffle.filter(Shuffle.ALL_ITEMS);
        }
    }

    /**
     * If any of the arrays in the `filters` property have a length of more than zero,
     * that means there is an active filter.
     * @return {boolean}
     */
    hasActiveFilters() {
        return Object.keys(this.filters).some((key) => {
            return this.filters[key].length > 0;
        }, this);
    }

    /**
     * Determine whether an element passes the current filters.
     * @param {Element} element Element to test.
     * @return {boolean} Whether it satisfies all current filters.
     */
    itemPassesFilters(element) {
        const categories = this.filters.categories;
        const category = element.dataset.category;

        // If there are active color filters and this color is not in that array.
        if (categories.length > 0 && !categories.includes(category)) {
            return false;
        }

        return true;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    if ($('.js-shuffle').length) {
        window.categoryFilter = new CategoryFilter(document.querySelector('.js-shuffle'));
    }
});


function detectDevice() {
    const mobileWidth = 578;
    let htmlWidth = $("html").width();

    if (htmlWidth < mobileWidth) {
        $('html').addClass('is-mobile');
    } else {
        $('html').removeClass('is-mobile');
    }
}

function toggleMenuBtn() {
    if ($('html').hasClass('is-mobile')) {
        $('.js-toggle-mobile').attr("data-toggle", "dropdown");
    } else {
        $('.js-toggle-mobile').removeAttr("data-toggle")
    }
}

function calcMenuMobile() {
    if ($('html').hasClass('is-mobile')) {
        let htmlH = $('html').height();
        let navbarH = $('.navbar').height();
        let navCollapse = htmlH - navbarH - 10;
        $('.navbar-collapse').css('min-height', navCollapse);
    } else {
        $('.navbar-collapse').css('min-height', 0);
    }
}
